package com.spring.petclinic.bootstrap;

import com.spring.petclinic.model.Owner;
import com.spring.petclinic.model.Vet;
import com.spring.petclinic.services.OwnerService;
import com.spring.petclinic.services.VetService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class DataLoader implements CommandLineRunner {

    private OwnerService ownerService;
    private VetService vetService;

    public DataLoader(OwnerService ownerService, VetService vetService) {
        this.ownerService = ownerService;
        this.vetService = vetService;
    }

    @Override
    public void run(String... args) {
        final Owner owner1 = new Owner();
        owner1.setFirstName("Aleksandra");
        owner1.setLastName("Petkova");
        ownerService.save(owner1);

        final Owner owner2 = new Owner();
        owner2.setFirstName("Ivan");
        owner2.setLastName("Pavlov");
        ownerService.save(owner2);

        final Owner owner3 = new Owner();
        owner3.setFirstName("Ivan");
        owner3.setLastName("Simionov");
        ownerService.save(owner3);

        final Vet vet1 = new Vet();
        vet1.setFirstName("Sofija");
        vet1.setLastName("Petroska");
        vetService.save(vet1);

        final Vet vet2 = new Vet();
        vet2.setFirstName("Gjurgjica");
        vet2.setLastName("Minova");
        vetService.save(vet2);
    }
}
