package com.spring.petclinic.services;

import com.spring.petclinic.model.Owner;

public interface OwnerService extends CrudService<Owner, Long> {
    Owner findByLastName(final String lastName);
}
