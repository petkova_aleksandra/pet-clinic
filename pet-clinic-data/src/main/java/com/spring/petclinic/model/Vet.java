package com.spring.petclinic.model;

public class Vet extends Person {

    private String firstName;
    private String lastName;

    public Vet() {
    }

    public Vet(String firstName, String lastName, String firstName1, String lastName1) {
        super(firstName, lastName);
        this.firstName = firstName1;
        this.lastName = lastName1;
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    @Override
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    @Override
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
