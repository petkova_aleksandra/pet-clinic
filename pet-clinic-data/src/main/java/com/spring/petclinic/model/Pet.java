package com.spring.petclinic.model;

import java.time.LocalDate;

public class Pet extends BaseEntity{

    private LocalDate birthDate;
    private PetType type;
    private Owner owner;

    public Pet() {
    }

    public Pet(LocalDate birthDate, PetType type, Owner owner) {
        this.birthDate = birthDate;
        this.type = type;
        this.owner = owner;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public PetType getType() {
        return type;
    }

    public void setType(PetType type) {
        this.type = type;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }
}
